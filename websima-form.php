<?php
/*
* Template Name: Websima Form
*/
?>
<div class="container my-4 py-3">
<?php get_header(); ?>
</div>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    global $wpdb;

    $first_name = $_REQUEST['first_name'];
    $last_name = $_REQUEST['last_name'];
    $full_name = $first_name ." ". $last_name;
    $phone_number = $_REQUEST['phone_number'];
    $email = $_REQUEST['email'];
    $natioal_code = $_REQUEST['natioal_code'];
    $category = $_REQUEST['category'];
    $datepicker = $_REQUEST['datepicker'];
    $dateen = $_REQUEST['dateen'];
    $time = $_REQUEST['time'];
    $desc = $_REQUEST['desc'];

    $wpdb->show_errors();

    $tags = $_POST['post_tags'];

    // Add the content of the form to $post as an array
    $new_post = array(
        'post_title'    => $full_name,
        'post_content'  => $desc,
        'post_category' => array($_POST['cat']),  // Usable for custom taxonomies too
        'tags_input'    => array($tags),
        'post_status'   => 'draft',           // Choose: publish, preview, future, draft, etc.
        'post_type' => 'websima'  //'post',page' or use a custom post type if you want to
    );
    //save the new post
    $pid = wp_insert_post($new_post);


    // Update the meta field.
    update_post_meta( $pid, 'first_name', $first_name );
    update_post_meta( $pid, 'last_name', $last_name );
    update_post_meta( $pid, 'phone_number', $phone_number );
    update_post_meta( $pid, 'email', $email );
    update_post_meta( $pid, 'natioal_code', $natioal_code );
    update_post_meta( $pid, 'category', $category );
    update_post_meta( $pid, 'datepicker', $datepicker );
    update_post_meta( $pid, 'dateen', $dateen );
    update_post_meta( $pid, 'time', $time );
    update_post_meta( $pid, 'desc', $desc );

    echo "<div class=\"container my-4\">";
    echo "<div class=\"alert alert-success\">";
    echo "Form Submit Success";
    echo "</div>";
    echo "</div>";

}else {
    ?>

    <div class="container my-4 py-3">

        <form onsubmit="return submit_websima_form()" autocomplete="off" id="websima-form" class="needs-validation" action ="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">

        <div class="row">
            <div class="col-md-4 my-3">
                <label for="first_name" class="form-label">First name</label>
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" required>
            </div>
            <div class="col-md-4 my-3">
                <label for="last_name" class="form-label">Last name</label>
                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" required>
            </div>
            <div class="col-md-4 my-3">
                <label for="phone_number" class="form-label">Phone Number</label>
                <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" required>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4 my-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
            </div>
            <div class="col-md-4 my-3">
                <label for="natioal_code" class="form-label">Natioal Code</label>
                <input type="text" class="form-control" name="natioal_code" id="natioal_code" placeholder="Natioal Code" required>
            </div>
            <div class="col-md-4 my-3">
                <label for="category" class="form-label">Category</label>
                <select name="category" id="category" class="form-control">
                    <option value="cat1">cat1</option>
                    <option value="cat2">cat2</option>
                    <option value="cat3">cat3</option>
                    <option value="cat4">cat4</option>
                    <option value="cat5">cat5</option>
                </select>
            </div>
        </div>


            <div class="row">
                <div class="col-md-6 my-3">
                    <label for="datepicker" class="form-label">Date</label>
                    <div class="input-group">
                        <span class="input-group-text cursor-pointer" id="datep" role="button">📅</span>
                        <input type="text" class="form-control" placeholder="Date" data-name="dtp1-text" id="datepicker" name="datepicker">
                        <input type="text" class="form-control d-none" placeholder="Date" data-name="dtp1-date" id="dateen" name="dateen">
                    </div>
                </div>
                <div class="col-md-6 my-3">
                    <label for="time" class="form-label">Time</label>
                    <select name="time" id="time" class="form-control">
                        <option value="time1">time1</option>
                        <option value="time2">time2</option>
                        <option value="time3">time3</option>
                        <option value="time4">time4</option>
                        <option value="time5">time5</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 my-3">
                    <label for="time" class="form-label">Desc</label>
                    <textarea rows="3" class="form-control" name="desc" id="desc" placeholder="Description" required></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-2">
                    <button type="submit" class="btn btn-success w-100">Submit Form</button>
                </div>
            </div>

        </form>
    </div>
<?php } get_footer(); ?>
