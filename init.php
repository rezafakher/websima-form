<?php

/*
 * Plugin Name: Websima Form
 * Description: Websima Form
 * Author: Reza Fakher
 * Author URI: fakherweb.ir
 */

class websima{

    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * The array of templates that this plugin tracks.
     */
    protected $templates;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {

        if ( null == self::$instance ) {
            self::$instance = new websima();
        }

        return self::$instance;

    }

    /**
     * Initializes the plugin by setting filters and administration functions.
     */
    private function __construct() {

        $this->templates = array();


         // Add a filter to the attributes metabox to inject template into the cache.
         if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

            // 4.6 and older
            add_filter(
                'page_attributes_dropdown_pages_args',
                array( $this, 'register_project_templates' )
            );

        } else {

            // Add a filter to the wp 4.7 version attributes metabox
            add_filter(
                'theme_page_templates', array( $this, 'add_new_template' )
            );

        }

        // Add a filter to the save post to inject out template into the page cache
        add_filter(
            'wp_insert_post_data',
            array( $this, 'register_project_templates' )
        );


        // Add a filter to the template include to determine if the page has our
        // template assigned and return it's path
        add_filter(
            'template_include',
            array( $this, 'view_project_template')
        );


        // Add your templates to this array.
        $this->templates = array(
            'websima-form.php' => 'Websima Form',
        );

    }

    /**
     * Adds our template to the page dropdown
     *
     */
    public function add_new_template( $posts_templates ) {
        $posts_templates = array_merge( $posts_templates, $this->templates );
        return $posts_templates;
    }

    /**
     * Adds our template to the pages cache in order to trick WordPress
     * into thinking the template file exists where it doens't really exist.
     */
    public function register_project_templates( $atts ) {

        // Create the key used for the themes cache
        $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

        // Retrieve the cache list.
        // If it doesn't exist, or it's empty prepare an array
        $templates = wp_get_theme()->get_page_templates();
        if ( empty( $templates ) ) {
            $templates = array();
        }

        // New cache, therefore remove the old one
        wp_cache_delete( $cache_key , 'themes');

        // Now add our template to the list of templates by merging our templates
        // with the existing templates array from the cache.
        $templates = array_merge( $templates, $this->templates );

        // Add the modified cache to allow WordPress to pick it up for listing
        // available templates
        wp_cache_add( $cache_key, $templates, 'themes', 1800 );

        return $atts;

    }

    /**
     * Checks if the template is assigned to the page
     */
    public function view_project_template( $template ) {

        // Get global post
        global $post;

        // Return template if post is empty
        if ( ! $post ) {
            return $template;
        }

        // Return default template if we don't have a custom one defined
        if ( ! isset( $this->templates[get_post_meta(
                $post->ID, '_wp_page_template', true
            )] ) ) {
            return $template;
        }

        $file = plugin_dir_path( __FILE__ ). get_post_meta(
                $post->ID, '_wp_page_template', true
            );

        // Just to be safe, we check if the file exist first
        if ( file_exists( $file ) ) {
            return $file;
        } else {
            echo $file;
        }

        // Return template
        return $template;

    }

}

// Register Custom Post Type
function websima_post_type() {
	$labels = array(
		'name'                  => _x( 'websimas', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'websima', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Websima Form', 'text_domain' ),
		'name_admin_bar'        => __( 'Websima Form', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'websima', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', '' ),
		'taxonomies'            => array( '' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'websima', $args );

}
add_action( 'init', 'websima_post_type', 0 );


/**
 * Calls the class on the post edit screen.
 */
function websima_meta_box() {
    new metaBoxClass();
}
 
if ( is_admin() ) {
    add_action( 'load-post.php',     'websima_meta_box' );
    add_action( 'load-post-new.php', 'websima_meta_box' );
}
 
/**
 * The Meta Box Class.
 */
class metaBoxClass {
 
    /**
     * Hook into the appropriate actions when the class is constructed.
     */
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
    }
 
    /**
     * Adds the meta box container.
     */
    public function add_meta_box( $post_type ) {
        // Limit meta box to certain post types.
        $post_types = array( 'websima' );
 
        if ( in_array( $post_type, $post_types ) ) {
            add_meta_box(
                'websima_meta_box',
                __( 'Websima Meta Box', 'textdomain' ),
                array( $this, 'render_meta_box_content' ),
                $post_type,
                'advanced',
                'high'
            );
        }
    }
 
    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_content( $post ) {
 
        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
  
        // Display the form, using the current value.
        $first_name = get_post_meta( $post->ID, "first_name", true );
        $last_name = get_post_meta( $post->ID, "last_name", true );
        $phone_number = get_post_meta( $post->ID, "phone_number", true );
        $email = get_post_meta( $post->ID, "email", true );
        $natioal_code = get_post_meta( $post->ID, "natioal_code", true );
        $category = get_post_meta( $post->ID, "category", true );
        $datepicker = get_post_meta( $post->ID, "datepicker", true );
        $dateen = get_post_meta( $post->ID, "dateen", true );
        $time = get_post_meta( $post->ID, "time", true );
        $desc = get_post_meta( $post->ID, "desc", true );
        $status = get_post_meta( $post->ID, "status", true );
?>

<table class="widefat striped">

    <tr>
        <td>First Name: </td>
        <td><?php echo $first_name; ?></td>
    </tr>
    <tr>
        <td>Last Name: </td>
        <td><?php echo $last_name; ?></td>
    </tr>
    <tr>
        <td>Phone Number: </td>
        <td><?php echo $phone_number; ?></td>
    </tr>
    <tr>
        <td>Email: </td>
        <td><?php echo $email; ?></td>
    </tr>
    <tr>
        <td>Natioal Code: </td>
        <td><?php echo $natioal_code; ?></td>
    </tr>
    <tr>
        <td>Category: </td>
        <td><?php echo $category; ?></td>
    </tr>
    <tr>
        <td>Datepicker: </td>
        <td><?php echo $datepicker; ?></td>
        <td><?php echo $dateen; ?></td>
    </tr>
    <tr>
        <td>Time: </td>
        <td><?php echo $time; ?></td>
    </tr>
    <tr>
        <td>Description: </td>
        <td><?php echo $desc; ?></td>
    </tr>
    <tr>
        <td>Status: </td>
        <td><?php echo get_post_status(); ?></td>
    </tr>
    <tr>
        <td>Time Create: </td>
        <td><?php echo get_the_date(); ?></td>
    </tr>
</table>

<?php
    }
}

function wpbootstrap_enqueue_styles() {
    wp_enqueue_style( 'bootstrap', plugins_url( '/css/bootstrap.min.css', __FILE__ ));
    wp_enqueue_style( 'persian-datepicker', plugins_url( '/css/persian-datepicker.css', __FILE__ ));

    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', plugins_url( '/js/jquery.min.js', __FILE__ ));
    wp_enqueue_script( 'popper', plugins_url( '/js/popper.min.js', __FILE__ ), array(), false, true);
    wp_enqueue_script( 'bootstrap-js', plugins_url( '/js/bootstrap.js', __FILE__ ), array(), false, true);
    wp_enqueue_script( 'datepicker-persian', plugins_url( '/js/persian-datepicker.js', __FILE__ ), array(), false, true);
    wp_enqueue_script( 'custom', plugins_url( '/js/custom.js', __FILE__ ), array(), false, true);
    }
    add_action('wp_enqueue_scripts', 'wpbootstrap_enqueue_styles');

add_action( 'plugins_loaded', array( 'websima', 'get_instance' ) );
register_activation_hook(__FILE__, 'websima_plugin_activation');
add_action( 'admin_menu', 'websima_menu' );


function websima_menu() {
    add_menu_page( 'Websima', 'Websima', 'manage_options', 'websima-admin', 'websima_admin', 'dashicons-clipboard'  );
}

function websima_plugin_activation() {
    websima_meta_box();
    websima_post_type();
}

function websima_admin(){
    require plugin_dir_path( __FILE__ ).'websima-admin.php';
}

?>