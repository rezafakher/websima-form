const dtp1Instance = new mds.MdsPersianDateTimePicker(document.getElementById('datep'), {
    targetTextSelector: '[data-name="dtp1-text"]',
    targetDateSelector: '[data-name="dtp1-date"]',
});